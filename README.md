# emObject-Morphology-Extension

An emObject based extension for morphology-aware phenotyping.


## Installation
Clone the `emMorphExtension.py` module to your local environment. Make sure the `morphology_demo.ipynb` notebook is able to find the path to this module.

## Usage
Extract morphology features, perform expression and morphology normalization, and phenotype cell populations using Leiden clustering with your emObject.