from collections import OrderedDict
from skimage.measure import regionprops
import pandas as pd
import numpy as np
from scipy import stats
from emobject.emlayer import BaseLayer
import igraph as ig
import leidenalg as la
from sklearn.neighbors import kneighbors_graph

def get_morphology_from_mask(labeled_image):

    # compute region properties of each mask
    rprops = regionprops(labeled_image)

    # list of feature names
    featname_map = OrderedDict({
        'CELL_ID': 'label',
        'm_Area': 'area',
        'm_MajorAxisLength': 'axis_major_length',
        'm_MinorAxisLength': 'axis_minor_length',
        'm_Circularity': None,
        'm_Eccentricity': 'eccentricity',
        'm_MinorMajorAxisRatio': None,
        'm_Extent': 'extent',
        'm_Solidity': 'solidity',
    })
    feature_list = featname_map.keys()
    mapped_feats = [k for k, v in featname_map.items() if v is not None]

    # create pandas.DataFrame with the features
    numFeatures = len(feature_list)
    numLabels = len(rprops)
    mdata = pd.DataFrame(np.zeros((numLabels, numFeatures)),
                         columns=feature_list)

    for i, nprop in enumerate(rprops):

        # get features from skimage.measure.regionprops
        for name in mapped_feats:
            mdata.at[i, name] = nprop[featname_map[name]]

        # compute circularity
        numerator = 4 * np.pi * nprop.area
        denominator = nprop.perimeter ** 2
        if denominator > 0:
            mdata.at[i, 'm_Circularity'] = numerator / denominator

        # compute MinorMajorAxisRatio'
        if nprop.axis_major_length > 0:
            mdata.at[i, 'm_MinorMajorAxisRatio'] = \
                nprop.axis_minor_length / nprop.axis_major_length
        else:
            mdata.at[i, 'm_MinorMajorAxisRatio'] = 1

    # set CELL_ID as index
    mdata['CELL_ID'] = mdata['CELL_ID'].astype(int)  # make CELL_ID interger
    mdata = mdata.set_index('CELL_ID')

    return mdata


def add_expr_morph_data_to_emo(emo):

    # add normalized expression data to .data
    data_asinh = np.arcsinh(emo.data)
    data_zscore = stats.zscore(data_asinh, axis=0)
    normed_layer = BaseLayer(data=data_zscore, 
                             name='normed')
    emo.add(normed_layer)


    # add normalized morphology to .obs
    seg_mask = emo.mask.mloc('segmentation_mask')
    mdata = get_morphology_from_mask(seg_mask)
    mdata_zscore = stats.zscore(mdata, axis=0)
    col_names = mdata_zscore.columns
    mdata_np = mdata_zscore.to_numpy()
    emo.add_anno(attr='obs',
                 value=mdata_np,
                 name=col_names)

    return emo


def add_expr_morph_data(ex):
    ex_generator = ex.apply(emo_names=None,
                            func=add_expr_morph_data_to_emo)
    emo_list = [E for E in ex_generator]
    
    return emo_list


def leiden_clustering(data, k):

    # Construct KNN graph from data
    knn_graph = kneighbors_graph(data, n_neighbors=k)
    sources, targets = knn_graph.nonzero()
    weights = knn_graph[sources, targets]
    if isinstance(weights, np.matrix):
        weights = weights.A1
    g = ig.Graph(directed=False)
    
    g.add_vertices(knn_graph.shape[0])  # each observation is a node
    edges = list(zip(sources, targets))
    g.add_edges(edges)
    
    g.es['weight'] = weights
    weights = np.array(g.es["weight"]).astype(np.float64)

    # Leiden partition the graph
    partition = la.find_partition(g, la.ModularityVertexPartition)

    # attach Leiden label to data
    cluster_label = partition.membership
    leiden_label = pd.DataFrame(cluster_label, columns=['leiden'])
    data_labeled = pd.concat([data, leiden_label], axis=1)

    return data_labeled